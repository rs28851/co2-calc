import { allTransportationType, allTransportWithCo2Emission, unitOfDistanceType, allUnitOfDistance, outputUnitType } from "../service/TransportationDataInterface";

export function checkTransportMethod (trasnportMethod: string): allTransportationType | undefined {
    if (trasnportMethod in allTransportWithCo2Emission) {
        return trasnportMethod as allTransportationType;
    } else {
        return undefined
    }
}

export function checkUnitOfDistance(unitOfDistance: string): unitOfDistanceType | undefined {
    if (unitOfDistance in allUnitOfDistance) {
        return unitOfDistance as unitOfDistanceType;
    } else {
        return undefined
    }
}

export function checkOutputUnit(output: string): outputUnitType | undefined {
    if (output in allUnitOfDistance) {
        return output as outputUnitType;
    } else {
        return undefined
    }
}