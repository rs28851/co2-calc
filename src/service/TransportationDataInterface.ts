/**
 * This defines all the small cars co2 emission per passaneger per km in gram
 */
export const smallCarsCo2Emission = {
    'small-diesel-car': '142g',
    'small-petrol-car': '154g',
    'small-plugin-hybrid-car': '73g',
    'small-electric-car': '50g'
}

/**
 * This defines all the medium cars co2 emission per passaneger per km in gram
 */
export const mediumCarsCo2Emission = {
    'medium-diesel-car': '171g',
    'medium-petrol-car': '192g',
    'medium-plugin-hybrid-car': '110g',
    'medium-electric-car': '58g'
}
/**
 * This defines all the large cars co2 emission per passaneger per km in gram
 */
export const largeCarsCo2Emission = {
    'large-diesel-car': '209g',
    'large-petrol-car': '282g',
    'large-plugin-hybrid-car': '126g',
    'large-electric-car': '73g'
}

/**
 * This defines co2 emission for all the transportation type per passaneger per km in gram
 */
export const allTransportWithCo2Emission = {
    ...smallCarsCo2Emission,
    ...mediumCarsCo2Emission,
    ...largeCarsCo2Emission,
    bus: '27g',
    train: '6g'
}
/**
 * contains all the transportation types
 */
export type allTransportationType = keyof typeof allTransportWithCo2Emission;

export const allUnitOfDistance = ['km', 'm'] as const;
export type unitOfDistanceType = typeof allUnitOfDistance[number];

export const allOutputUnit = ['kg', 'g'] as const;
export type outputUnitType = typeof allOutputUnit[number];